package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Element;
import com.tinkerpop.blueprints.Index;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientEdge;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;

/**
 *
 * @author K Adithyan
 */
public class OGraphTx 
{
	private static OrientGraphFactory orientGraphFactory;

	private static void initFactory()
	{
		System.out.println("starting.......");
		long t1 = System.currentTimeMillis();
		String url = "remote:localhost/inventory";
		//String url = "plocal:/root/adb";
		orientGraphFactory = new OrientGraphFactory(url).setupPool(1, 4);
		long t2 = System.currentTimeMillis();
		System.out.println("time taken for gettting graph factory = " + (t2 - t1) + " ms");
		OrientGraph g = orientGraphFactory.getTx();

		long t3 = System.currentTimeMillis();
		System.out.println("time taken for gettting graph object = " + (t3 - t2) + " ms");
		ODatabaseDocumentTx gg = g.getRawGraph();

		//g.dropKeyIndex("key", Vertex.class);
		//g.dropKeyIndex("key", Edge.class);
		OIndex<?> vindex = gg.getMetadata().getIndexManager().getIndex("invnode.key");
		if (vindex != null)
			System.out.println("index = " + vindex.getName() + "|" + vindex.getType() + "|" + vindex.getConfiguration().toJSON());

		OIndex<?> eindex = gg.getMetadata().getIndexManager().getIndex("me-ptp.key");
		if (eindex != null)
			System.out.println("index = " + eindex.getName() + "|" + eindex.getType() + "|" + eindex.getConfiguration().toJSON());

		Iterable<Index<? extends Element>> indices = g.getIndices();

		for (Index<? extends Element> i : indices)
		{
			System.out.println("index = " + i);
		}

		boolean exists = gg.getMetadata().getIndexManager().existsIndex("invnode.key");
		System.out.println("index exists = " + exists);
		if (!exists)
		{
			g.createKeyIndex("key", Vertex.class, new Parameter("class", "invnode"), new Parameter("type", "unique_hash_index"));
			System.out.println("v index created");
			g.createKeyIndex("key", Edge.class, new Parameter("class", "me-ptp"), new Parameter("type", "unique_hash_index"));
			System.out.println("e index created");
		}

		long t4 = System.currentTimeMillis();
		System.out.println("time taken for creating index = " + (t4 - t3) + " ms");
		System.out.println("total time for getGraph = " + (t4 - t1) + " ms");

		//if (true)
		//	System.exit(0);
	}

	static void addNodeAndEdge()
	{
		initFactory();
		OrientGraph og = orientGraphFactory.getTx();
		OrientVertex v12 = og.addVertex("class:invnode", "key", 12, "name", "adithyan", "age", 32);
		OrientVertex v13 = og.addVertex("class:invnode", "key", 13, "name", "adithyan", "age", 32);
		System.out.println("v12 = " + v12.getRecord().toJSON());
		System.out.println("v13 = " + v13.getRecord().toJSON());
		
		v12.save();
		v13.save();
	
		OrientVertex ov12 = (OrientVertex) og.getVertexByKey("invnode.key", 12);
		OrientVertex ov13 = (OrientVertex) og.getVertexByKey("invnode.key", 13);

		System.out.println("ov12 = " + ov12.getRecord().toJSON());
		System.out.println("ov13 = " + ov13.getRecord().toJSON());
		
		OrientEdge e = ov12.addEdge("me-ptp", ov13, "key", "e1");
		System.out.println("e = " + e.getRecord().toJSON());
	}
	
	public static void main(String[] args)
	{
		addNodeAndEdge();
	}
}
