package adi.orientdb.eval;

import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;

/**
 *
 * @author K Adithyan
 */
public class ORemote 
{
	public static void main(String[] args)
	{
		read();
	}

	private static void read()
	{
		OrientGraph og = new OrientGraph("remote:localhost/GratefulDeadConcerts");
		System.out.println("nodes = " + og.countVertices());
		
		Iterable<Vertex> vertices = og.getVertices();
		
		for (Vertex vertex : vertices)
		{
			System.out.println("v = " + vertex.getPropertyKeys());
		}
	}
	

}
