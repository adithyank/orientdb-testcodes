package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexDefinition;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.index.OPropertyIndexDefinition;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.record.impl.ORecordBytes;
import java.io.FileInputStream;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.Arrays;
import java.util.Date;

/**
 *
 * @author K Adithyan
 */
public class OBinary 
{
	private static ODatabaseDocumentTx db()
	{
		System.out.println("starting.....");
		ODatabaseDocumentTx base = new ODatabaseDocumentTx("plocal:/root/okvdb");

		if (!base.exists())
		{
			System.out.println("creating....");
			base.create();
			base.getMetadata().getSecurity().createUser("a", "a", "admin");
		}

		System.out.println("post create");

		System.out.println("base = " + base);
		base.close();

		ODatabaseDocumentPool pool = new ODatabaseDocumentPool("plocal:/root/okvdb", "a", "a");

		ODatabaseDocumentTx db = pool.acquire();

		if (db.exists())
			System.out.println("db exists");
		else
			System.out.println("db does not exist");

		System.out.println("db = " + db);

		if (!db.getMetadata().getSchema().existsClass("docnode"))
		{
			OClass createClass = db.getMetadata().getSchema().createClass("docnode");
			System.out.println("create class = " + createClass);
		}

		OIndexManagerProxy im = db.getMetadata().getIndexManager();

		if (!im.existsIndex("docnode.name"))
		{
			OIndexDefinition def = new OPropertyIndexDefinition("docnode", "name", OType.STRING);
			im.createIndex("docnode.name", "UNIQUE_HASH_INDEX", def, null, null, null, "SBTREE");
			System.out.println("index docnode.name created");
		}
		else
		{
			System.out.println("index docnode.name exists");
		}

		long doccount = db.countClass("docnode");
		System.out.println("doccount of docnode = " + doccount);

		return db;
	}
	
	private static void read() throws Exception
	{
		db();
		ODatabaseDocumentPool pool = new ODatabaseDocumentPool("plocal:/root/okvdb", "a", "a");

		System.out.println("got db");
		System.out.println("got index");
		
		long startTime = System.currentTimeMillis();
		for (int i = 0; i < 650000; i += 100)
		{
			
			OIndex<?> index = pool.acquire().getMetadata().getIndexManager().getIndex("docnode.name");
			String key = "value-" + i;
			ORecordId rid = (ORecordId) index.get(key);
			ODocument doc = rid.getRecord();
			ORecordBytes orb = doc.field("binary");
			byte[] toStream = orb.toStream();
			//System.out.println("length = " + toStream.length);
		}
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken for read = " + (endTime - startTime) + " ms");
	}

	private static void write() throws Exception
	{
		db();
		ODatabaseDocumentPool pool = new ODatabaseDocumentPool("plocal:/root/okvdb", "a", "a");		
		pool.setup(1, 10);

		System.out.println("building byte array....");
		FileInputStream fis = new FileInputStream("/adi/tmp/downloads/ckt-issues/ckt-588079-07-Oct-2014-12-40-31-Member");
		FileChannel channel = fis.getChannel();
		ByteBuffer bb = ByteBuffer.allocate((int) channel.size());
		channel.read(bb);
		byte[] arr = bb.array();
		
		//byte[] arr = byteArray(ois.readObject());

		//byte[] arr = new byte[100 * 1024];
		
		Arrays.fill(arr, (byte) 97);
		
		System.out.println("starting.....");
		
		long startTime = System.currentTimeMillis();
		
		for (int i = 0; i < 650000; i++)
		{
			if (i % 1000 == 0)
				System.out.println(new Date() + " : starting..... " + i);
		
			ODatabaseDocumentTx db = pool.acquire();
			
			ORecordBytes orb = new ORecordBytes(arr);
			
			db.begin();
			
			ODocument doc = new ODocument("docnode");
			doc.field("name", "value-" + i);
			doc.field("binary", orb);
			
			doc.save();
			db.commit();
			db.close();
		}
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken for storing records = " + (endTime - startTime) + " ms");
	}
	
	public static void main(String[] args) throws Exception
	{
		//write();
		read();
	}

}
