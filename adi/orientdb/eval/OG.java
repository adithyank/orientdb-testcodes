package adi.orientdb.eval;

import com.orientechnologies.orient.core.Orient;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

/**
 *
 * @author K Adithyan
 */
public class OG 
{
	private static final String INDEX_NODE_CLASS = "node";

	private final OrientGraphFactory graphFactory;

	public OG() throws Exception
	{
		int maxThreadCount = 10;

		//String dbPath = "/home/cygnet/builds/spotlight/CygNet/graph/rcasia/inventory";
		String dbPath = "/root/ddbb";
		String url = "plocal:" + dbPath;

		System.out.println("Configured DB Path : " + dbPath + " : url = " + url + ", maxThreadCount = " + maxThreadCount);
		

		graphFactory = new OrientGraphFactory(url);
		graphFactory.setupPool(1, maxThreadCount);

		OrientGraph g = graphFactory.getTx();

		try
		{
			ODatabaseDocumentTx db = g.getRawGraph();

			OIndexManagerProxy im = db.getMetadata().getIndexManager();

			String vertexIndexName = INDEX_NODE_CLASS + ".uniqueKey";

			if (im.existsIndex(vertexIndexName))
			{
				System.out.println("Vertex Index already exists for " + INDEX_NODE_CLASS);
			}
			else
			{
				System.out.println("Creating Verted index : " + vertexIndexName);
				g.createKeyIndex("uniqueKey", Vertex.class, new Parameter("class", INDEX_NODE_CLASS), new Parameter("type", "unique_hash_index"));
				System.out.println("Created Verted index : " + vertexIndexName);
			}

			List<String> relTypes = Arrays.asList(new String[] {"RELTYPE1", "RELTYPE2"});
			System.out.println("relTypes.size() = " + relTypes.size() + ", relTypes = " + relTypes);

			for (String relType : relTypes)
			{
				String edgeIndexName = relType + "." + "uniqueKey";
				if (im.existsIndex(edgeIndexName))
				{
					System.out.println("Edge Index already exists for " + relType);
				}
				else
				{
					System.out.println("Creating edge index : " + edgeIndexName);
					g.createKeyIndex("uniqueKey", Edge.class, new Parameter("class", relType), new Parameter("type", "unique_hash_index"), new Parameter("algorithm", "HASH"));
					System.out.println("Created edge index : " + edgeIndexName);
				}
				
				OIndex o = im.getIndex(edgeIndexName);
				System.out.println("index = " + o.getConfiguration().toJSON());
			}
		}
		catch (Exception e)
		{
			System.out.println("Exception while doing init operations : " + e);
		}
		finally
		{
			g.shutdown();
			System.out.println("graph shutdown done.....");
		}
		
		System.out.println("creating graphs...");
		
		OrientGraph tx1 = graphFactory.getTx();
		OrientGraph tx2 = graphFactory.getTx();
		OrientGraph tx3 = graphFactory.getTx();
		OrientGraph tx4 = graphFactory.getTx();
		
		tx1.addVertex(null, "a", "v1", "b", "v2");
		//tx1.shutdown();
		//tx2.shutdown();
		//tx3.shutdown();
		//tx4.shutdown();
		
		System.out.println("sleeping.....");
		Thread.sleep(1000);
		System.out.println("woke up");
		
		System.out.println("factory not closed");

		Orient.instance().shutdown();
		
		System.out.println("orient shutdown done");
		
		System.out.println("waiting.............");

		Thread.sleep(1000000);
		
		graphFactory.close();
	}
	
	private static void printHooks() throws Exception
	{
		Class clazz = Class.forName("java.lang.ApplicationShutdownHooks");
		Field field = clazz.getDeclaredField("hooks");
		field.setAccessible(true);
		Object hooks = field.get(null);
		
		Map map = (Map) hooks;
		System.out.println("class = " + hooks.getClass().getName());
		System.out.println(hooks); //hooks is a Map<Thread, Thread>
		
		for (Object o : map.entrySet())
		{
			Entry e = (Map.Entry)o;
			java.lang.Object key = e.getKey();
			java.lang.Object value = e.getValue();
			
			System.out.println(e + " =========== " + key.getClass().getName() + " -- " + value.getClass().getName());
		}
		
	}
	
	public static void main(String[] args) throws Exception
	{
		new OG();
	}		
}
