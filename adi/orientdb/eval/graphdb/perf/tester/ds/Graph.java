package adi.orientdb.eval.graphdb.perf.tester.ds;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author K Adithyan
 */
public class Graph 
{
	private final Map<String, Node> nodes = new HashMap<>();
	private final Map<String, Rel> edges = new HashMap<>();

	public Node addNode(String type)
	{
		Node n = new Node(type);
		nodes.put(n.getUniqueKey(), n);
		return n;
	}

	public Rel addEdge(String type, Node start, Node end)
	{
		Rel e = new Rel(type, start.getUniqueKey(), end.getUniqueKey());
		edges.put(e.getUniqueKey(), e);
		return e;
	}
	
	public List<Node> getAllNodes()
	{
		return new ArrayList<>(nodes.values());
	}
	
	public List<Rel> getAllEdges()
	{
		return new ArrayList<>(edges.values());
	}
	
	public int nodeSize()
	{
		return nodes.size();
	}
	
	public int edgeSize()
	{
		return edges.size();
	}
}
