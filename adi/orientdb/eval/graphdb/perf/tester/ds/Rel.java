package adi.orientdb.eval.graphdb.perf.tester.ds;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author K Adithyan
 */
public class Rel extends GraphEntity
{
	private final String startNodeUniqueKey;
	private final String endNodeUniqueKey;

	private final String attribute1 = "value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1";
	private final String attribute2 = "value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-2";

	public Rel(String type, String startNodeUniqueKey, String endNodeUniqueKey)
	{
		
		super(type, getMD5Checksum(startNodeUniqueKey + "-" + type + "-" + endNodeUniqueKey));
		this.startNodeUniqueKey = startNodeUniqueKey;
		this.endNodeUniqueKey = endNodeUniqueKey;
	}

	public Map<String, Object> getProperties()
	{
		Map<String, Object> map = new HashMap<>();
		map.put(UNIQUE_KEY, getUniqueKey());
		map.put("type", getType());
		map.put("attribute1", attribute1);
		map.put("attribute2", attribute2);
		return map;
	}

	public String getStartNodeUniqueKey()
	{
		return startNodeUniqueKey;
	}

	public String getEndNodeUniqueKey()
	{
		return endNodeUniqueKey;
	}

	public String getAttribute1()
	{
		return attribute1;
	}

	public String getAttribute2()
	{
		return attribute2;
	}
}
