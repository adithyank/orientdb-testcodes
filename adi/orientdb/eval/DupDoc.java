package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.OPartitionedDatabasePool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.iterator.ORecordIteratorClass;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author K Adithyan
 */
public class DupDoc
{
	static final String URL = "plocal:/tmp/plocaldb";
	//static final String URL = "remote:/localhost/plocaldb";
	static final String DATATYPE = "docnode";
	static final String KEYFIELD = "name";
	
	static boolean WRITE_MODE = true;
	
	public static void main(String[] args) throws Exception
	{
		if (WRITE_MODE)
			write();
		else
			read();
		
		System.out.println("sleeping...");
		Thread.sleep(1000000);
	}
	
	private static ODatabaseDocumentTx db() throws InterruptedException, ExecutionException
	{
		System.out.println("starting.....");

		if (URL.startsWith("plocal"))
		{
			try (ODatabaseDocumentTx base = new ODatabaseDocumentTx(URL))
			{
				if (!base.exists())
				{
					System.out.println("creating....");
					base.create();
					base.getMetadata().getSecurity().createUser("root", "root", "admin");
				}

				base.close();
			}
		}
		
		OPartitionedDatabasePool pool = new OPartitionedDatabasePool(URL, "root", "root");

		ODatabaseDocumentTx db = pool.acquire();
		
		String indexName = DATATYPE + "." + KEYFIELD;
		
		if (!db.getMetadata().getSchema().existsClass(DATATYPE))
		{
			OClass dataTypeClass = db.getMetadata().getSchema().createClass(DATATYPE);
			dataTypeClass.createProperty(KEYFIELD, OType.STRING);
			dataTypeClass.createIndex(indexName, OClass.INDEX_TYPE.UNIQUE_HASH_INDEX, KEYFIELD);

			System.out.println("created class = " + dataTypeClass);
		}
		
		final OIndexManagerProxy im = db.getMetadata().getIndexManager();
		OIndex index = im.getIndex(indexName);
		System.out.println("index = " + index.getConfiguration().toJSON());
		
		long doccount = db.countClass(DATATYPE);
		System.out.println("doccount of " + DATATYPE + " = " + doccount);

		return db;
	}

	private static void write() throws Exception
	{
		ODatabaseDocumentTx db = db();
		db.begin();

		/**************node creation starts**********/
		String KEY = "name-1";
		Map<String, Object> props = new HashMap<>();
		props.put("date", "" + new Date());
		props.put("name", KEY);
		props.put("prop1", "prop1-" + KEY);
		props.put("prop2", "prop2-" + KEY);
		
		ODocument doc = new ODocument("docnode");
		doc.fromMap(props);
		doc.save();
		/**************node creation ends**********/

		
		System.out.println("node added");
		System.out.println("doccount of " + DATATYPE + " = " + db.countClass("docnode"));
		db.commit();
		db.rollback();
		System.out.println("doccount of " + DATATYPE + " = " + db.countClass("docnode"));
		db.close();
		System.out.println("doccount of " + DATATYPE + " = " + db.countClass("docnode"));
	}
	
	private static void read() throws Exception
	{
		ODatabaseDocumentTx db = db();
		ORecordIteratorClass<ODocument> iter = db.browseClass(DATATYPE);

		while (iter.hasNext())
			System.out.println("node = " + iter.next().toJSON());
	}
}
