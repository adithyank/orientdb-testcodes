package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexDefinition;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.index.OPropertyIndexDefinition;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.tx.OTransaction;
import com.orientechnologies.orient.server.OServer;
import com.orientechnologies.orient.server.OServerMain;
import com.orientechnologies.orient.server.config.OServerConfiguration;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author K Adithyan
 */
public class ODocTx
{
	static final String URL = "plocal:plocaldb";
	
	static String CLASS = "patpor";
	static String PATTERNNAME = "pattern";
	static String PORTIONNAME = "portion";
	
	static int TOTAL_TX = 1;
	static int DOC_PER_TX = 1;
	
	static Random random = new Random();
	
	private static OIndex index;
	
	public static void main(String[] args) throws Exception
	{
		//startEmbeddedServer();
		//write();
		
		System.out.println("sleeping...");
		Thread.sleep(100000);
		read();
	}
	
	private static void startEmbeddedServer() throws Exception
	{
		OServerConfiguration c = new OServerConfiguration();
		
		OServer server = OServerMain.create();
		server.startup(new File("/mnt/data/adi/installation-files/common/graphdb/orientdb/orient-2.0-M3/orientdb-community-2.0-M3/config/orientdb-server-config.xml"));
		server.activate();
	}

	private static ODatabaseDocumentTx db() throws InterruptedException, ExecutionException
	{
		System.out.println("starting.....");
		ODatabaseDocumentTx base = new ODatabaseDocumentTx(URL);
		
		if (!base.exists())
		{
			System.out.println("creating....");
			base.create();
			base.getMetadata().getSecurity().createUser("a", "a", "admin");
		}
		
		System.out.println("post create");
		
		System.out.println("base = " + base);
		base.close();
		
		ODatabaseDocumentPool pool = new ODatabaseDocumentPool(URL, "a", "a");

		ODatabaseDocumentTx db = pool.acquire();
		
		if (db.exists())
			System.out.println("db exists");
		else
			System.out.println("db does not exist");
		
		System.out.println("db = " + db);

		if (!db.getMetadata().getSchema().existsClass("docnode"))
		{
			OClass createClass = db.getMetadata().getSchema().createClass("docnode");
			System.out.println("create class = " + createClass);
		}
		
		final OIndexManagerProxy im = db.getMetadata().getIndexManager();

		if (!im.existsIndex("docnode.name"))
		{
			OIndexDefinition def = new OPropertyIndexDefinition("docnode", "name", OType.STRING);
			im.createIndex("docnode.name", "UNIQUE_HASH_INDEX", def, null, null, null, "SBTREE");
			System.out.println("index docnode.name created");
		}
		else
		{
			System.out.println("index docnode.name exists");
		}
		
		index = im.getIndex("docnode.name");
		
		long doccount = db.countClass("docnode");
		System.out.println("doccount of docnode = " + doccount);

		return db;
	}

	private static List<Object> list()
	{
		List<Object> l = new ArrayList<>();
		
		for (int i = 0; i < 10; i++)
			l.add("india difw er 2i34 2l3    ej faawoie rlwke rliw rlkaj fjh sjdfiwe oriweour wekjr oweio if sdkf owe krw erow erkw erow erlw row erlw eor wlerwoeirlwekrj lkwefffasflksdjfliwejrlkwejrlwejrowieroiweruoiweurowie rwlke rl flas dflas flwierjlwkejrliawerjlkafjlaksfj laskfjlaksfjlaskdfjlai erlkj");
		
		return l;
	}
	
	private static Map map()
	{
		List<String> list = new ArrayList<>();
		list.add("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		list.add("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
		list.add("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
		list.add("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");

		Map<String, List<String>> map = new HashMap<>();
		map.put("one", list);
		map.put("two", list);
		map.put("three", list);
		map.put("four", list);
		map.put("five", list);
		map.put("six", list);
		map.put("seven", list);
		map.put("eight", list);
		map.put("nine", list);
		map.put("ten", list);
		return map;
	}
	
	private static String storeNode(String name)
	{
		Map<String, Object> props = new HashMap<>();
		props.put("date", "" + new Date());
		
		String uk = "adithyan-" + name;
		
		props.put("name", uk);
		props.put("bandwidthservicelayerrate", "lfajk ldfkjalsdkfjaoeirulwkj29374234jhkleflkajfksdhflkjhasf,mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate1", "lfajk ldfkjalsdkfjaoeirulwkj29374234jhkleflkajfksdhflkjhasf,mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate2", "lfajk ldfkjalsdkfdsjfaoeirulwfdkj29374234jhkleflkajfwerksdhflkjhasf,42mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate3", "lfajk ldfkjalsdkfdsjfaoeirulwfdkj29374234jhkleflkajfwerksdhflkjhasf,42mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate4", "lfajk ldfkjalsdkfdsjfaoeirulwfdkj29374234jhkleflkajfwerksdhflkjhasf,42mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate5", "lfajk ldfkjalsdkfdsjfaoeirulwfdkj29374234jhkleflkajfwerksdhflkjhasf,42mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("list", list());
		props.put("map", map());
		
		ODocument doc = new ODocument("docnode");
		
		doc.fields(props);
		doc.save();
		
		return uk;
	}
	
	private static void write() throws Exception
	{
		long t1 = System.currentTimeMillis();
		ODatabaseDocumentTx db = db();
		long t2 = System.currentTimeMillis();
		System.out.println("time taken for db = " + (t2 - t1) + " ms");

		String previousName = null;
		
		for (int o = 0; o < TOTAL_TX; o++)
		{
			long t11 = System.currentTimeMillis();
			long t12 = System.currentTimeMillis();

			for (int i = 0; i < DOC_PER_TX; i++)
			{
				String name = o + "-" + i;

				db.begin(OTransaction.TXTYPE.OPTIMISTIC);
				String uk = storeNode(name);

				ODocument get = ((ORecordId) index.get(uk)).getRecord();
				System.out.println("from index for " + uk + " : " + get.toMap());

				db.commit();
				previousName = "adithyan-" + name;
			}

			long t13 = System.currentTimeMillis();
			long t14 = System.currentTimeMillis();
			System.out.println(new Date() + " : completed............... " + ((o + 1) * DOC_PER_TX) + " in " + (t14 - t2) + " ms. total time for this " + DOC_PER_TX + " docs = " + (t14 - t11) + " ms. obj creation+save = " + (t13 - t12) + " ms");
			//System.out.println("time taken for tx begin = " + (t12 - t11) + " ms, obj creation & save = " + (t13 - t12) + " ms, commit " + (t14 - t13) + " ms. total time = " + (t14 - t11) + " ms");
		}

		long t5 = System.currentTimeMillis();
		System.out.println("overall time = " + (t5 - t1) + " ms");
		db.close();
	}
	
	private static void read() throws Exception
	{
		ODatabaseDocumentTx db = db();
		
		OIndexManagerProxy im = db.getMetadata().getIndexManager();
		OIndex<?> index = im.getIndex("docnode.name");
		
		long t2 = System.currentTimeMillis();

		int count = 0;

		for (int i = 0; i < 10; i++)
		{
			System.out.println("-----------------------------------------------------------------------");
			String key = "adithyan-" + random.nextInt(TOTAL_TX) + "-" + random.nextInt(DOC_PER_TX);

			
			
			
			ORecordId rid = (ORecordId) index.get(key);
			ODocument doc = rid.getRecord();

			String[] fns = doc.fieldNames();
			for (String fn : fns)
				System.out.println(fn + " --> " + doc.field(fn));
		}
		
		
//		for (int o = 0; o < TOTAL_TX; o++)
//		{
//			long t11 = System.currentTimeMillis();
//			db.begin(OTransaction.TXTYPE.OPTIMISTIC);
//			long t12 = System.currentTimeMillis();
//
//			for (int i = 0; i < DOC_PER_TX; i += 100)
//			{
//				count++;
//				String key = "adithyan-" + o + "-" + i;
//				ORecordId rid = (ORecordId) index.get(key);
//				ODocument doc = rid.getRecord();
//
//				String[] fns = doc.fieldNames();
//				for (String fn : fns)
//					doc.field(fn);
//			}
//
//			long t13 = System.currentTimeMillis();
//			db.commit();
//			long t14 = System.currentTimeMillis();
//		}
		long t3 = System.currentTimeMillis();
		System.out.println(new Date() + " : completed reading " + count + " in " + (t3 - t2) + " ms");
	}
}
