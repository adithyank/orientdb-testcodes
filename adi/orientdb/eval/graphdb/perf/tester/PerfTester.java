package adi.orientdb.eval.graphdb.perf.tester;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.metadata.schema.OSchema;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import adi.orientdb.eval.graphdb.perf.tester.GraphBuilder.EdgeType;
import adi.orientdb.eval.graphdb.perf.tester.ds.Graph;
import adi.orientdb.eval.graphdb.perf.tester.ds.GraphEntity;
import adi.orientdb.eval.graphdb.perf.tester.ds.Node;
import adi.orientdb.eval.graphdb.perf.tester.ds.Rel;

/**
 *
 * @author K Adithyan
 */
public class PerfTester 
{
	public static final String INDEX_NODE_CLASS = "node";

	private static final int BATCHES = 7000;
	private static final int SIZE_PER_BATCH = 230;
	private static final String URL = "plocal:/ssd2/ogperf";
	
	private static OrientGraphFactory graphFactory = null;

	private static OIndex nodeIndex;
	private static final Map<String, OIndex> edgeIndices = new ConcurrentHashMap<>();
	
	private static long totalGraphDBNodes = 0;
	private static long totalGraphDBEdges = 0;

	private static long nodeId;
	
	public static long newNodeId()
	{
		return ++nodeId;
	}
	
	private static void initialize() throws Exception
	{
		graphFactory = new OrientGraphFactory(URL);

		OrientGraph g = graphFactory.getTx();

		try
		{
			ODatabaseDocumentTx db = g.getRawGraph();

			OIndexManagerProxy im = db.getMetadata().getIndexManager();

			OSchema schema = db.getMetadata().getSchema();
			if (schema.existsClass(INDEX_NODE_CLASS))
				System.out.println("Count of Nodes available in DB = " + db.countClass(INDEX_NODE_CLASS));
			else
				System.out.println("Class " + INDEX_NODE_CLASS + " not found");

			String vertexIndexName = INDEX_NODE_CLASS + "." + GraphEntity.UNIQUE_KEY;

			if (im.existsIndex(vertexIndexName))
			{
				System.out.println("Vertex Index already exists for " + vertexIndexName);
			}
			else
			{
				System.out.println("Creating Vertex index : " + vertexIndexName);
				g.createKeyIndex(GraphEntity.UNIQUE_KEY, Vertex.class, new Parameter("class", INDEX_NODE_CLASS), new Parameter("type", "unique_hash_index"));
				System.out.println("Created Vertex index : " + vertexIndexName);
			}
			
			totalGraphDBNodes = db.countClass(INDEX_NODE_CLASS);
			nodeId = totalGraphDBNodes;
			
			nodeIndex = im.getIndex(vertexIndexName);

			for (EdgeType et : EdgeType.values())
			{
				String edgeType = et.name();
				if (schema.existsClass(edgeType))
					System.out.println("Count of Edge[" + edgeType + "] available in DB = " + db.countClass(edgeType));
				else
					System.out.println("Class " + edgeType + " not found");

				String edgeIndexName = edgeType + "." + GraphEntity.UNIQUE_KEY;
				if (im.existsIndex(edgeIndexName))
				{
					System.out.println("Edge Index already exists for " + edgeIndexName);
				}
				else
				{
					System.out.println("Creating edge index : " + edgeIndexName);
					g.createKeyIndex(GraphEntity.UNIQUE_KEY, Edge.class, new Parameter("class", edgeType), new Parameter("type", "unique_hash_index"));
					System.out.println("Created edge index : " + edgeIndexName);
				}

				totalGraphDBEdges += db.countClass(edgeType);
				
				OIndex ei = im.getIndex(edgeIndexName);
				System.out.println("Putting in index map : " + ei.getConfiguration().toJSON());
				edgeIndices.put(edgeType, ei);
			}

			g.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			g.rollback();
		}
		finally
		{
			g.shutdown();
		}
	}

	private static void build()
	{
		for (int b = 0; b < BATCHES; b++)
		{
			Count thisBatchCount = new Count();
			
			long startTime = System.currentTimeMillis();

			for (int s = 0; s < SIZE_PER_BATCH; s++)
			{
				Graph g = GraphBuilder.newGraph();
				addInGraphDB(g, thisBatchCount);
			}

			totalGraphDBNodes += thisBatchCount.nodeCount;
			totalGraphDBEdges += thisBatchCount.edgeCount;
			
			long endTime = System.currentTimeMillis();
			System.out.println(new Date() + " : Time taken to store " + thisBatchCount.nodeCount + " nodes and " + thisBatchCount.edgeCount + " edges = " + (endTime - startTime) + " ms. Current graph db size = " + totalGraphDBNodes + " nodes and " + totalGraphDBEdges + " edges");
		}
	}

	private static void addInGraphDB(Graph g, Count thisBatchCount)
	{
		OrientGraph og = null;

		try
		{
			og = graphFactory.getTx();
			GraphSaver.storeGraph(g, og, thisBatchCount);
			og.commit();
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
			if (og != null)
				og.rollback();
		}
		finally
		{
			if (og != null)
				og.shutdown();
		}
	}

	public static class GraphSaver
	{
		static boolean nodeExists(String uniqueKey)
		{
			return nodeIndex.contains(uniqueKey);
		}
		
		static boolean edgeExists(String edgeType, String uniqueKey)
		{
			return edgeIndices.get(edgeType).contains(uniqueKey);
		}
		
		public static void storeGraph(Graph g, OrientGraph orientGraph, Count thisBatchCount) throws Exception
		{
			for (Node node : g.getAllNodes())
			{
				if (nodeExists(node.getUniqueKey()))
					continue;
				
				storeNode(node, orientGraph);
				thisBatchCount.nodeCount++;
			}
			
			for (Rel rel : g.getAllEdges())
			{
				if (edgeExists(rel.getType(), rel.getUniqueKey()))
					continue;
				
				storeEdge(rel, orientGraph);
				thisBatchCount.edgeCount++;
			}
		}
		
		private static void storeNode(Node node, OrientGraph orientGraph)
		{
			orientGraph.addVertex("class:" + INDEX_NODE_CLASS, node.getProperties());
		}
		
		private static void storeEdge(Rel rel, OrientGraph orientGraph)
		{
			OrientVertex startVertex = getVertex(rel.getStartNodeUniqueKey(), orientGraph);
			OrientVertex endVertex = getVertex(rel.getEndNodeUniqueKey(), orientGraph);
			startVertex.addEdge(rel.getType(), endVertex, new Object[] {rel.getProperties()});
		}
		
		private static OrientVertex getVertex(String uniqueKey, OrientGraph orientGraph)
		{
			return (OrientVertex) orientGraph.getVertexByKey(INDEX_NODE_CLASS + "." + GraphEntity.UNIQUE_KEY, uniqueKey);
		}
	}
	
	private static class Count
	{
		int nodeCount = 0;
		int edgeCount = 0;
	}

	public static void main(String[] args) throws Exception
	{
		Graph graph = GraphBuilder.newGraph();
		System.out.println("size of one graph : " + graph.nodeSize() + " nodes and " + graph.edgeSize() + " edges");
		System.out.println("estimated graph db size = " + BATCHES * SIZE_PER_BATCH * graph.nodeSize() + " nodes and " + BATCHES * SIZE_PER_BATCH * graph.edgeSize() + " edges");
		
		System.out.println("initializing.....");
		initialize();
		System.out.println("initialized...");
		
		System.out.println("total graph db size now = " + totalGraphDBNodes + " nodes and " + totalGraphDBEdges + " edges");
		
		System.out.println("starting to build....");
		build();
	}
}
