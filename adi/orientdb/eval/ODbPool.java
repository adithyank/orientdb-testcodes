package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Element;
import com.tinkerpop.blueprints.Index;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientEdge;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import java.util.Set;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ODbPool 
{
	private static OrientGraphFactory factory;

	static String getNodeName(String name)
	{
		return "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD-" + name;
	}
	
	private static OrientVertex getNode(OrientGraph g, String name)
	{
		String fullname = getNodeName(name);
		//System.out.println("getNode() called : " + name);
		Iterable<Vertex> vertices = g.getVertices("invnode.key", fullname);
		boolean available = vertices.iterator().hasNext();
		//System.out.println("available(" + name + ") = " + available);
		
		OrientVertex hub = g.addVertex("class:invnode", "key", fullname);
		return hub;
	}

	public static void main(String[] args)
	{
		write();
		//read();
	}

	public static void read()
	{
		setFactory();
		OrientGraph g = factory.getTx();
		System.out.println("g = " + g);
		System.out.println("node count = " + g.countVertices());

		String[] inputs = {"me10", "me1000", "me3453"};
		
		for (String input : inputs)
		{
			Iterable<Vertex> vs = g.getVertices("invnode.key", getNodeName(input));
			Vertex v = vs.iterator().next();

			long t2 = System.currentTimeMillis();
			Iterable<Edge> edges = v.getEdges(Direction.OUT);

			int ec = 0;
			for (Edge ed : edges)
			{
				OrientEdge oe = (OrientEdge) ed;
				//System.out.println("edge = " + oe.getRecord().toJSON());
				oe.getRecord().toJSON();
				ec++;
			}

			long t3 = System.currentTimeMillis();

			System.out.println("read time for edges in me10 = " + (t3 - t2) + " ms, count = " + ec);
		}

		System.out.println("OVER>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		
		//OrientVertex vv = (OrientVertex) g.getVertices("key", "me100").iterator().next();
		long t4 = System.currentTimeMillis();
		OrientEdge e1 = (OrientEdge) g.getEdges("me-ptp.key", "rel-me-1-ptp-100").iterator().next();
		OrientEdge e2 = (OrientEdge) g.getEdges("me-ptp.key", "rel-me-1-ptp-200").iterator().next();

		Set<String> propertyKeys = e1.getPropertyKeys();
		for (String propertyKey : propertyKeys)
		{
			e1.getProperty(propertyKey);
		}

		propertyKeys = e2.getPropertyKeys();
		for (String propertyKey : propertyKeys)
		{
			e2.getProperty(propertyKey);
		}

		System.out.println(e1);
		System.out.println(e2);

		long t5 = System.currentTimeMillis();

		System.out.println("read time for edges in me10 = " + (t5 - t4) + " ms");
		System.out.println("done....");
	}

	private static void setFactory()
	{
		System.out.println("starting.......");
		long t1 = System.currentTimeMillis();
		String url = "plocal:/root/successdb";
		factory = new OrientGraphFactory(url).setupPool(1, 2);
		long t2 = System.currentTimeMillis();
		System.out.println("time taken for gettting graph factory = " + (t2 - t1) + " ms");
		OrientGraph g = factory.getTx();

		long t3 = System.currentTimeMillis();
		System.out.println("time taken for gettting graph object = " + (t3 - t2) + " ms");
		ODatabaseDocumentTx gg = g.getRawGraph();

		//g.dropKeyIndex("key", Vertex.class);
		//g.dropKeyIndex("key", Edge.class);
		OIndex<?> vindex = gg.getMetadata().getIndexManager().getIndex("invnode.key");
		if (vindex != null)
			System.out.println("index = " + vindex.getName() + "|" + vindex.getType() + "|" + vindex.getConfiguration().toJSON());
		
		OIndex<?> eindex = gg.getMetadata().getIndexManager().getIndex("me-ptp.key");
		if (eindex != null)
			System.out.println("index = " + eindex.getName() + "|" + eindex.getType() + "|" + eindex.getConfiguration().toJSON());
		
		Iterable<Index<? extends Element>> indices = g.getIndices();
		
		for (Index<? extends Element> i : indices)
		{
			System.out.println("index = " + i);
		}

		boolean exists = gg.getMetadata().getIndexManager().existsIndex("invnode.key");
		System.out.println("index exists = " + exists);
		if (!exists)
		{
			g.createKeyIndex("key", Vertex.class, new Parameter("class", "invnode"), new Parameter("type", "unique_hash_index"));
			System.out.println("v index created");
			g.createKeyIndex("key", Edge.class, new Parameter("class", "me-ptp"), new Parameter("type", "unique_hash_index"));
			System.out.println("e index created");
		}
		
		long t4  = System.currentTimeMillis();
		System.out.println("time taken for creating index = " + (t4 - t3) + " ms");
		System.out.println("total time for getGraph = " + (t4 - t1) + " ms");
		
		//if (true)
		//	System.exit(0);
	}

	public static void write()
	{
		ExecutorService es = Executors.newFixedThreadPool(5);
		setFactory();
		
		es.submit(new Adder("1"));
		sleep(500);
		es.submit(new Adder("2"));
		sleep(500);
		es.submit(new Adder("3"));
		sleep(500);
		es.submit(new Adder("4"));
		sleep(500);
		es.submit(new Adder("5"));
	}
	
	private static void sleep(long ms)
	{
		try
		{
			Thread.sleep(ms);
		}
		catch (InterruptedException ex)
		{
		}
	}
	
	private static class Adder implements Runnable
	{
		String name;

		public Adder(String name)
		{
			this.name = name;
		}
		
		void sop(String s)
		{
			System.out.println(name + " : " + s);
		}
		
		@Override
		public void run()
		{
			try
			{
				sop("called... waiting to get graph");
				OrientGraph og = factory.getTx();
				
				sop("got graph");
				og.addVertex("class:invnode", "key", "name-1-" + System.nanoTime(), "value", "value1");
				sop("vertex added");
				sleep(1000);
				sop("sleep over");
				og.commit();
				sop("done....");
				og.shutdown();
			}
			catch (Exception e)
			{
				e.printStackTrace();
			}
		}
	}
}
