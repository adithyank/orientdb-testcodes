/** OLinkTester.java -

 * @version      $Name$
 * @module       in.co.nmsworks
 *
 * @purpose
 * @see
 *
 * @author   Manikandan E (emanikandan@nmsworks.co.in)
 *
 * @created  Dec 17, 2014
 * $Id$
 *
 * @bugs
 *
 * Copyright 2014-2015 NMSWorks Software Pvt Ltd. All rights reserved.
 * NMSWorks PROPRIETARY/CONFIDENTIAL. Use is subject to licence terms.
 */

package adi.orientdb.eval;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexDefinition;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.index.OPropertyIndexDefinition;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.tx.OTransaction;
import java.util.HashSet;
import java.util.Set;

public class OLinkTester {

	static String CLASS[] = {"Portion", "Pattern"};
	static String PATTERNNAME = "pattern";
	static String PORTIONNAME = "portion";

	static int TOTAL_TX = 10;
	static int DOC_PER_TX = 1000;

	static Random random = new Random();

	private static ODatabaseDocumentTx db()
	{
		System.out.println("starting.....");
		ODatabaseDocumentTx base = new ODatabaseDocumentTx("plocal:/tmp/ori");

		if (!base.exists())
		{
			System.out.println("creating....");
			base.create();
			base.getMetadata().getSecurity().createUser("a", "a", "admin");
		}

		System.out.println("post create");

		System.out.println("base = " + base);
		base.close();

		ODatabaseDocumentPool pool = new ODatabaseDocumentPool("plocal:/home/admin/ori", "a", "a");

		ODatabaseDocumentTx db = pool.acquire();

		if (db.exists())
			System.out.println("db exists");
		else
			System.out.println("db does not exist");

		System.out.println("db = " + db);

		for (String klass : CLASS) {

			if (!db.getMetadata().getSchema().existsClass(klass))
			{
				if(klass.equals("Pattern"))
				{
					OClass createClass = db.getMetadata().getSchema().createClass(klass);
					createClass.createProperty("name", OType.STRING);
					createClass.createProperty("portion", OType.LINKSET, db.getMetadata().getSchema().getClass("Portion"));
					System.out.println("create class = " + createClass);
				}
				else
				{
					OClass createClass = db.getMetadata().getSchema().createClass(klass);
					createClass.createProperty("name", OType.STRING);
                    createClass.createProperty("value", OType.STRING);
					System.out.println("create class = " + createClass);
				}
			}
			
			db.getMetadata().getSchema().save();

			OIndexManagerProxy im = db.getMetadata().getIndexManager();

			if (!im.existsIndex(klass+".name"))
			{
				OIndexDefinition def = new OPropertyIndexDefinition(klass, "name", OType.STRING);
				im.createIndex(klass+".name", "UNIQUE_HASH_INDEX", def, null, null, null, "SBTREE");
				System.out.println("index "+klass+".name"+ "created");
			}
			else
			{
				System.out.println("index "+klass+".name"+ "exists");
			}

			long doccount = db.countClass(klass);
			System.out.println("doccount of"+klass+" = " + doccount);
		}

		return db;
	}

	private static void storePortion(String name, String patternName, ODatabaseDocumentTx db)
	{
		Map<String, Object> props = new HashMap<String, Object>();

		props.put("name", "mani-" + name);
		props.put("value", "" + new Date());

		ODocument portion = new ODocument("Portion");
		portion.fromMap(props);
		portion.save();
		
		OIndexManagerProxy im = db.getMetadata().getIndexManager();
		OIndex<ORecordId> index = (OIndex<ORecordId>) im.getIndex("Pattern.name");
		ORecordId get = index.get(patternName);
		ODocument pattern = get.getRecord();
		Set set = pattern.field("portion");

		if (set == null)
			set = new HashSet();
		
		set.add(portion);
		pattern.field("portion", set);
		pattern.save();
	}

	private static void write()
	{
		long t1 = System.currentTimeMillis();
		ODatabaseDocumentTx db = db();
		long t2 = System.currentTimeMillis();
		System.out.println("time taken for db = " + (t2 - t1) + " ms");
		
		db.begin();
		ODocument pattern = new ODocument("Pattern");
		pattern.field("name", "mani");
		pattern.field("obj", "objecttype");
		pattern.save();
		db.commit();
		
		long t3 = System.currentTimeMillis();
		for (int o = 0; o < 1; o++)
		{
			long t11 = System.currentTimeMillis();
			db.begin(OTransaction.TXTYPE.OPTIMISTIC);
			long t12 = System.currentTimeMillis();
	
			for (int i = 0; i < 10000; i++)
			{				
				String name = o + "-" + i;
				storePortion(name, "mani", db);
			}
			
			long t13 = System.currentTimeMillis();
			db.commit();
			long t14 = System.currentTimeMillis();
			System.out.println("Pattern complete");
		//	System.out.println(new Date() + " : completed............... " + ((o + 1) * 100) + " in " + (t14 - t2) + " ms. total time for this 100 docs = " + (t14 - t11) + " ms. obj creation+save = " + (t13 - t12) + " ms");
		}
		
		long t5 = System.currentTimeMillis();
		System.out.println("overall time = " + (t5 - t3) + " ms");
		db.close();
	}

	public static void main(String[] args) 
	{
		write();
		System.out.println("done........");
	}


}


/**
 * $Log$
 *
 */