package adi.orientdb.eval;

import com.orientechnologies.orient.client.remote.OServerAdmin;
import com.orientechnologies.orient.core.db.OPartitionedDatabasePool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.tx.OTransaction;
import com.orientechnologies.orient.server.OServer;
import com.orientechnologies.orient.server.OServerMain;
import com.orientechnologies.orient.server.config.OServerConfiguration;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

/**
 *
 * @author K Adithyan
 */
public class ODocTester 
{
	//static final String CREATE_URL = "remote:localhost/odocdb";
	//static final String URLL = "remote:localhost/odocdb";
	//static final String CREATE_URL = "plocal:/root/netbeansProjects/projects/OrientDBTester/databases/odocdb";
	//static final String URLL = "plocal:/root/netbeansProjects/projects/OrientDBTester/databases/odocdb";
	
	static final String CREATE_URL = "plocal:/tmp/odocdb1";
	static final String URLL = "plocal:/tmp/odocdb1";
	
	static ThreadPoolExecutor tp = (ThreadPoolExecutor) Executors.newFixedThreadPool(1);
	
	static String CLASS = "patpor";
	static String PATTERNNAME = "pattern";
	static String PORTIONNAME = "portion";
	
	static int TOTAL_TX = 2;
	static int DOC_PER_TX = 1000;
	
	static Random random = new Random();
	
	private static OIndex sameThreadIndex;
	
	public static void main(String[] args) throws Exception
	{
		//startEmbeddedServer();
		//write();
		
		read();
		System.out.println("sleeping...");
		Thread.sleep(1000000);
	}
	
	private static void startEmbeddedServer() throws Exception
	{
		OServerConfiguration c = new OServerConfiguration();
		
		OServer server = OServerMain.create();
		server.startup(new File("/mnt/data/adi/installation-files/common/graphdb/orientdb/orient-2.0-M3/orientdb-community-2.0-M3/config/orientdb-server-config.xml"));
		server.activate();
	
		System.out.println("embedded server actiavted....");
	}

	private static ODatabaseDocumentTx db() throws InterruptedException, ExecutionException, IOException
	{
		System.out.println("starting.....");
		ODatabaseDocumentTx base = new ODatabaseDocumentTx(CREATE_URL);
		
		if (CREATE_URL.startsWith("plocal"))
		{
			if (!base.exists())
			{
				System.out.println("creating....");
				base.create();
				base.getMetadata().getSecurity().createUser("root", "root", "admin");
			}
		}
		else
		{
			OServerAdmin osa = new OServerAdmin(CREATE_URL).connect("root", "root");
			
			if (osa.existsDatabase())
			{
				System.out.println("db already available : " + CREATE_URL);
			}
			else
			{
				osa.createDatabase("document", "plocal");
				ODatabaseDocumentTx rem = new ODatabaseDocumentTx(CREATE_URL);
				rem.open("root", "root");
				rem.getMetadata().getSecurity().createUser("root", "root", "admin");
				System.out.println("created db......");
			}
		}
		
		System.out.println("post create");
		
		System.out.println("base = " + base);
		base.close();
		
		OPartitionedDatabasePool pool = new OPartitionedDatabasePool(URLL, "root", "root");

		ODatabaseDocumentTx db = pool.acquire();

		if (db.exists())
			System.out.println("db exists");
		else
			System.out.println("db does not exist");
		
		System.out.println("db = " + db);

		createindex(db);
		final OIndexManagerProxy im = db.getMetadata().getIndexManager();

		sameThreadIndex = im.getIndex("docnode.name");
		
		System.out.println("index = " + sameThreadIndex.getConfiguration().toJSON());
		sameThreadIndex.delete();
		System.out.println("index deleted");
		
		createindex(db);
		sameThreadIndex = im.getIndex("docnode.name");
		System.out.println("index = " + sameThreadIndex.getConfiguration().toJSON());
		
		long doccount = db.countClass("docnode");
		System.out.println("doccount of docnode = " + doccount);

		return db;
	}
	
	private static void createindex(ODatabaseDocumentTx db)
	{
		boolean indexExists = db.getMetadata().getSchema().existsClass("docnode");
		System.out.println("indexExists = " + indexExists);
		if (!indexExists)
		{
			OClass createClass = db.getMetadata().getSchema().createClass("docnode");
			createClass.createProperty("name", OType.STRING);
			createClass.createIndex("docnode.name", OClass.INDEX_TYPE.UNIQUE_HASH_INDEX, "name");
			System.out.println("create class = " + createClass);
			System.out.println("index created.............");
		}

	}

	private static List<Object> list()
	{
		List<Object> l = new ArrayList<>();
		
		for (int i = 0; i < 10; i++)
			l.add("india difw er 2i34 2l3    ej faawoie rlwke rliw rlkaj fjh sjdfiwe oriweour wekjr oweio if sdkf owe krw erow erkw erow erlw row erlw eor wlerwoeirlwekrj lkwefffasflksdjfliwejrlkwejrlwejrowieroiweruoiweurowie rwlke rl flas dflas flwierjlwkejrliawerjlkafjlaksfj laskfjlaksfjlaskdfjlai erlkj");
		
		return l;
	}
	
	private static Map map()
	{
		List<String> list = new ArrayList<>();
		list.add("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
		list.add("BBBBBBBBBBBBBBBBBBBBBBBBBBBBBBBB");
		list.add("CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC");
		list.add("DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD");

		Map<String, List<String>> map = new HashMap<>();
		map.put("one", list);
		map.put("two", list);
		map.put("three", list);
		map.put("four", list);
		map.put("five", list);
		map.put("six", list);
		map.put("seven", list);
		map.put("eight", list);
		map.put("nine", list);
		map.put("ten", list);
		return map;
	}
	
	private static String storeNode(String name)
	{
		Map<String, Object> props = new HashMap<>();
		props.put("date", "" + new Date());
		
		String uk = "adithyan-" + name;
		
		props.put("name", uk);
		props.put("bandwidthservicelayerrate", "lfajk ldfkjalsdkfjaoeirulwkj29374234jhkleflkajfksdhflkjhasf,mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate1", "lfajk ldfkjalsdkfjaoeirulwkj29374234jhkleflkajfksdhflkjhasf,mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate2", "lfajk ldfkjalsdkfdsjfaoeirulwfdkj29374234jhkleflkajfwerksdhflkjhasf,42mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate3", "lfajk ldfkjalsdkfdsjfaoeirulwfdkj29374234jhkleflkajfwerksdhflkjhasf,42mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate4", "lfajk ldfkjalsdkfdsjfaoeirulwfdkj29374234jhkleflkajfwerksdhflkjhasf,42mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("bandwidthservicelayerrate5", "lfajk ldfkjalsdkfdsjfaoeirulwfdkj29374234jhkleflkajfwerksdhflkjhasf,42mdnasldkjfhajksdflkjasdfhlkajshdfkja");
		props.put("list", list());
		props.put("map", map());
		
		ODocument doc = new ODocument("docnode");
		
		doc.fromMap(props);
		doc.save();
		
		return uk;
	}
	
	private static void write() throws Exception
	{
		System.out.println("url = " + CREATE_URL);
		long t1 = System.currentTimeMillis();
		ODatabaseDocumentTx db = db();
		long t2 = System.currentTimeMillis();
		System.out.println("time taken for db = " + (t2 - t1) + " ms");

		for (int o = 0; o < TOTAL_TX; o++)
		{
			long t3 = System.currentTimeMillis();
			db.begin(OTransaction.TXTYPE.OPTIMISTIC);
			for (int i = 0; i < DOC_PER_TX; i++)
			{
				String name = o + "-" + i;
				storeNode(name);
			}

			long t4 = System.currentTimeMillis();
			
			try
			{
				System.out.println(new Date() + " : committing.....");
				db.commit();
			}
			catch (Exception e)
			{
				System.out.println(e.getMessage());
				System.out.println(new Date() + " : rolling back.........");
				db.rollback();
			}
			
			long t5 = System.currentTimeMillis();
			System.out.println(new Date() + " : object creation time = " + (t4 - t3) + " ms. time for commit " + DOC_PER_TX + " docs = " + (t5 - t4) + " ms. total = " + (t5 - t3) + " ms");
		}

		long t6 = System.currentTimeMillis();
		System.out.println("overall time = " + (t6 - t2) + " ms");
		db.close();
	}
	
	private static void read() throws Exception
	{
		ODatabaseDocumentTx db = db();
		
		OIndexManagerProxy im = db.getMetadata().getIndexManager();
		OIndex<?> index = im.getIndex("docnode.name");

		System.out.println("index = " + index.getClass().getName());
		
		long t2 = System.currentTimeMillis();

		int count = 0;

		for (int i = 0; i < 10; i++)
		{
			System.out.println("-----------------------------------------------------------------------");
			String key = "adithyan-" + random.nextInt(TOTAL_TX) + "-" + random.nextInt(DOC_PER_TX);
			
			ORecordId rid = (ORecordId) index.get(key);
			ODocument doc = rid.getRecord();

			String[] fns = doc.fieldNames();
			for (String fn : fns)
				System.out.println(fn + " --> " + doc.field(fn));
		}
		
		
//		for (int o = 0; o < TOTAL_TX; o++)
//		{
//			long t11 = System.currentTimeMillis();
//			db.begin(OTransaction.TXTYPE.OPTIMISTIC);
//			long t12 = System.currentTimeMillis();
//
//			for (int i = 0; i < DOC_PER_TX; i += 100)
//			{
//				count++;
//				String key = "adithyan-" + o + "-" + i;
//				ORecordId rid = (ORecordId) index.get(key);
//				ODocument doc = rid.getRecord();
//
//				String[] fns = doc.fieldNames();
//				for (String fn : fns)
//					doc.field(fn);
//			}
//
//			long t13 = System.currentTimeMillis();
//			db.commit();
//			long t14 = System.currentTimeMillis();
//		}
		long t3 = System.currentTimeMillis();
		System.out.println(new Date() + " : completed reading " + count + " in " + (t3 - t2) + " ms");
	}
}
