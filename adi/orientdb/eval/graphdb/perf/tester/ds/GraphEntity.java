package adi.orientdb.eval.graphdb.perf.tester.ds;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author K Adithyan
 */
public class GraphEntity 
{
	public static final String UNIQUE_KEY = "uniqueKey";
	
	private final String type;
	private final String uniqueKey;

	public GraphEntity(String type, String uniqueKey)
	{
		this.type = type;
		this.uniqueKey = uniqueKey;
	}

	public String getType()
	{
		return type;
	}

	public String getUniqueKey()
	{
		return uniqueKey;
	}

	public static String getMD5Checksum(String s)
	{
		try
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] result = md.digest(s.getBytes());
			return convertByteArrayToHexString(result);
		}
		catch (NoSuchAlgorithmException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	private static String convertByteArrayToHexString(byte[] array)
	{
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < array.length; i++)
		{
			String hex = Integer.toHexString(0xff & array[i]);
			if (hex.length() == 1)
			{
				sb.append('0');
			}
			sb.append(hex);
		}
		return sb.toString();
	}

	
}
