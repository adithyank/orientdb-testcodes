package adi.orientdb.eval.graphdb.perf.tester;

import adi.orientdb.eval.graphdb.perf.tester.ds.Graph;
import adi.orientdb.eval.graphdb.perf.tester.ds.Node;

/**
 *
 * @author K Adithyan
 */
public class GraphBuilder 
{
	public static Graph newGraph()
	{
		Graph g = new Graph();
		Node ckt = g.addNode(NodeType.CIRCUIT.name());
		Node mainSeq = addSequence(g);

		Node p1 = addPoint(g);
		Node p2 = addPoint(g);
		Node p3 = addPoint(g);
		Node p4 = addPoint(g);
		Node p5 = addPoint(g);
		Node p6 = addPoint(g);
		Node wp1 = addPoint(g);
		Node wp2 = addPoint(g);
		Node pp1 = addPoint(g);
		Node pp2 = addPoint(g);

		Node ne1 = addNE(g);
		Node ne2 = addNE(g);
		Node ne3 = addNE(g);
		Node ne4 = addNE(g);

		Node tl1 = addTL(g);
		Node tl2 = addTL(g);
		Node wtl1 = addTL(g);
		Node ptl1 = addTL(g);

		Node sncp = addSNCP(g);

		Node w = addSequence(g);
		Node p = addSequence(g);

		g.addEdge(EdgeType.CKT_SEQ.name(), ckt, mainSeq);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, p1);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, ne1);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, p2);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, tl1);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, p3);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, ne2);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, p4);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, tl2);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, p5);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, ne3);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, sncp);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, ne4);
		g.addEdge(EdgeType.SEQ_CHILD.name(), mainSeq, p6);

		g.addEdge(EdgeType.TL_POINT.name(), tl1, p2);
		g.addEdge(EdgeType.TL_POINT.name(), tl1, p3);
		g.addEdge(EdgeType.TL_POINT.name(), tl2, p4);
		g.addEdge(EdgeType.TL_POINT.name(), tl2, p5);
		g.addEdge(EdgeType.TL_POINT.name(), wtl1, wp1);
		g.addEdge(EdgeType.TL_POINT.name(), wtl1, wp2);
		g.addEdge(EdgeType.TL_POINT.name(), ptl1, pp1);
		g.addEdge(EdgeType.TL_POINT.name(), ptl1, pp2);
		
		g.addEdge(EdgeType.SNCP_START_CLIENT_POINT.name(), sncp, p5);
		g.addEdge(EdgeType.SNCP_END_CLIENT_POINT.name(), sncp, p6);
		
		g.addEdge(EdgeType.SNCP_W.name(), sncp, w);
		g.addEdge(EdgeType.SNCP_P.name(), sncp, p);

		g.addEdge(EdgeType.SEQ_CHILD.name(), w, wp1);
		g.addEdge(EdgeType.SEQ_CHILD.name(), w, wtl1);
		g.addEdge(EdgeType.SEQ_CHILD.name(), w, wp2);

		g.addEdge(EdgeType.SEQ_CHILD.name(), p, pp1);
		g.addEdge(EdgeType.SEQ_CHILD.name(), p, ptl1);
		g.addEdge(EdgeType.SEQ_CHILD.name(), p, pp2);
		
		g.addEdge(EdgeType.SEQ_S.name(), w, wp1);
		g.addEdge(EdgeType.SEQ_E.name(), w, wp2);

		g.addEdge(EdgeType.SEQ_S.name(), p, pp1);
		g.addEdge(EdgeType.SEQ_E.name(), p, pp2);

		Node[] points = new Node[] {p1, p2, p3, p4, p5, p6, wp1, wp2, pp1, pp2};

		for (int i = 0; i < points.length - 1; i++)
		{
			g.addEdge(EdgeType.DOWN_STR.name(), points[i], points[i + 1]);
			g.addEdge(EdgeType.REMOTE.name(), points[i], points[i + 1]);
			g.addEdge(EdgeType.OPP.name(), points[i], points[i + 1]);
		}
		
		return g;
	}

	private static Node addPoint(Graph g)
	{
		return g.addNode(NodeType.POINT.name());
	}

	private static Node addNE(Graph g)
	{
		return g.addNode(NodeType.NE.name());
	}

	private static Node addTL(Graph g)
	{
		return g.addNode(NodeType.TL.name());
	}

	private static Node addSequence(Graph g)
	{
		return g.addNode(NodeType.SEQUENCE.name());
	}

	private static Node addSNCP(Graph g)
	{
		return g.addNode(NodeType.SNCP.name());
	}

	public enum NodeType
	{
		CIRCUIT,
		SEQUENCE,
		NE,
		POINT,
		TL,
		SNCP
	}

	public enum EdgeType
	{
		CKT_SEQ,
		SEQ_CHILD,
		SNCP_W,
		SNCP_P,
		SNCP_START_CLIENT_POINT,
		SNCP_END_CLIENT_POINT,
		SEQ_S,
		SEQ_E,
		TL_POINT,
		DOWN_STR,
		REMOTE,
		OPP
	}
}
