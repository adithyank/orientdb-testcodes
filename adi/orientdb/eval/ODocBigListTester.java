package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexDefinition;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.index.OPropertyIndexDefinition;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author K Adithyan
 */
public class ODocBigListTester 
{
	private static ODatabaseDocumentTx db()
	{
		System.out.println("starting.....");
		ODatabaseDocumentTx base = new ODatabaseDocumentTx("plocal:/root/okvdb");

		if (!base.exists())
		{
			System.out.println("creating....");
			base.create();
			base.getMetadata().getSecurity().createUser("a", "a", "admin");
		}

		System.out.println("post create");

		System.out.println("base = " + base);
		base.close();

		ODatabaseDocumentPool pool = new ODatabaseDocumentPool("plocal:/root/okvdb", "a", "a");

		ODatabaseDocumentTx db = pool.acquire();

		if (db.exists())
			System.out.println("db exists");
		else
			System.out.println("db does not exist");

		System.out.println("db = " + db);

		if (!db.getMetadata().getSchema().existsClass("docnode"))
		{
			OClass createClass = db.getMetadata().getSchema().createClass("docnode");
			System.out.println("create class = " + createClass);
		}

		OIndexManagerProxy im = db.getMetadata().getIndexManager();

		if (!im.existsIndex("docnode.name"))
		{
			OIndexDefinition def = new OPropertyIndexDefinition("docnode", "name", OType.STRING);
			im.createIndex("docnode.name", "UNIQUE_HASH_INDEX", def, null, null, null, "SBTREE");
			System.out.println("index docnode.name created");
		}
		else
		{
			System.out.println("index docnode.name exists");
		}

		long doccount = db.countClass("docnode");
		System.out.println("doccount of docnode = " + doccount);

		return db;
	}

	private static void storeNode(String name)
	{
		Map<String, Object> props = new HashMap<>();
		props.put("date", "" + new Date());
		props.put("name", "adithyan-" + name);
		props.put("list", list());
		
		ODocument doc = new ODocument("docnode");
		doc.fromMap(props);
		doc.save();
	}
	
	private static List<Object> list()
	{
		List<Object> l = new ArrayList<>();
		
		for (int i = 0; i < 20000; i++)
			l.add("" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis() + "" + System.currentTimeMillis());
		
		return l;
	}
	
	private static void write()
	{
		ODatabaseDocumentTx db = db();
	
		System.out.println("db obtained....... starting.......");
		
		int DOCS = 2100;
		
		db.begin();
		
		long startTime = System.currentTimeMillis();
		for (int doc = 2000; doc < DOCS; doc++)
			storeNode("doc-" + doc);
		
		db.commit();
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken for storing " + DOCS + " records = " + (endTime - startTime) + " ms");
	}
	
	public static void update()
	{
		ODatabaseDocumentTx db = db();
	
		System.out.println("db obtained....... starting.......");

		OIndexManagerProxy im = db.getMetadata().getIndexManager();
		OIndex<?> index = im.getIndex("docnode.name");
		
		long startTime = System.currentTimeMillis();
		
		db.begin();
		
		ORecordId o = (ORecordId) index.get("adithyan-doc-2000");
		ODocument doc = o.getRecord();
		List<String> list = doc.field("list");
		
		System.out.println("field = " + list.getClass().getName());
		System.out.println("list.size = " + list.size());
		
		list.add("adithyan");
		doc.save();
		db.commit();
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken for one list update = " + (endTime - startTime) + " ms");
		
	}
	
	public static void main(String[] args)
	{
		//write();
		update();
		System.out.println("done");
	}
}
