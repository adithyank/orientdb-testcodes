package adi.orientdb.eval.graphdb.perf.tester.ds;

import java.util.HashMap;
import java.util.Map;
import adi.orientdb.eval.graphdb.perf.tester.PerfTester;

/**
 *
 * @author K Adithyan
 */
public class Node extends GraphEntity
{
	private final String attribute1 = "value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1";
	private final String attribute2 = "value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-2";
	private final String attribute3 = "value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-3";
	private final String attribute4 = "value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-4";
	private final String attribute5 = "value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-1-value-4";

	public Node(String type)
	{
		super(type, getMD5Checksum("nodenodenode-" + PerfTester.newNodeId()));
	}

	public Map<String, Object> getProperties()
	{
		Map<String, Object> map = new HashMap<>();
		map.put(UNIQUE_KEY, getUniqueKey());
		map.put("type", getType());
		map.put("attribute1", attribute1);
		map.put("attribute2", attribute2);
		map.put("attribute3", attribute3);
		map.put("attribute4", attribute4);
		map.put("attribute5", attribute5);
		return map;
	}

	public String getAttribute1()
	{
		return attribute1;
	}

	public String getAttribute2()
	{
		return attribute2;
	}

	public String getAttribute3()
	{
		return attribute3;
	}

	public String getAttribute4()
	{
		return attribute4;
	}

	public String getAttribute5()
	{
		return attribute5;
	}
	
	
}
