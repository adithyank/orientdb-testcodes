package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexDefinition;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.index.OPropertyIndexDefinition;
import com.orientechnologies.orient.core.iterator.ORecordIteratorClass;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;

/**
 *
 * @author K Adithyan
 */
public class ODocKill
{
	static final String URL = "plocal:/root/plocaldb";
	static final String DATATYPE = "docnode";
	static final String KEYFIELD = "name";
	
	static boolean WRITE_MODE = true;
	static ODatabaseDocumentPool pool;
	
	public static void main(String[] args) throws Exception
	{
		if (WRITE_MODE)
			write();
		else
			read();
		System.out.println("sleeping...");
		Thread.sleep(1000000);
	}
	
	private static ODatabaseDocumentTx db() throws InterruptedException, ExecutionException
	{
		System.out.println("starting.....");

		try (ODatabaseDocumentTx base = new ODatabaseDocumentTx(URL))
		{
			if (!base.exists())
			{
				System.out.println("creating....");
				base.create();
				base.getMetadata().getSecurity().createUser("a", "a", "admin");
			}

			base.close();
		}

		pool = new ODatabaseDocumentPool(URL, "a", "a");
		pool.setup(1, 10);
		
		ODatabaseDocumentTx db = pool.acquire();
		
		if (!db.getMetadata().getSchema().existsClass(DATATYPE))
		{
			OClass createClass = db.getMetadata().getSchema().createClass(DATATYPE);
			System.out.println("created class = " + createClass);
		}
		
		final OIndexManagerProxy im = db.getMetadata().getIndexManager();

		String indexName = DATATYPE + "." + KEYFIELD;
		if (!im.existsIndex(indexName))
		{
			OIndexDefinition def = new OPropertyIndexDefinition(DATATYPE, KEYFIELD, OType.STRING);
			im.createIndex(indexName, "UNIQUE_HASH_INDEX", def, null, null, null);
			System.out.println("index docnode.name created");
		}
		else
		{
			System.out.println("index docnode.name exists");
		}
		
		OIndex index = im.getIndex(indexName);
		System.out.println("index = " + index.getConfiguration().toJSON());
		
		long doccount = db.countClass("docnode");
		System.out.println("doccount of docnode = " + doccount);

		return db;
	}

	private static void write() throws Exception
	{
		ODatabaseDocumentTx db = db();

		for (int i = 0; i < 9; i++)
		{
			new Thread(new Runnable()
			{
				@Override
				public void run()
				{
					try
					{
						pool.acquire().begin();
					}
					catch (Exception ex)
					{
						ex.printStackTrace();
					}
				}
			}).start();
		}

		db.begin();
		
		/**************node creation starts**********/
		String key = "name-1";
		Map<String, Object> props = new HashMap<>();
		props.put("date", "" + new Date());
		props.put("name", key);
		props.put("prop1", "prop1-" + key);
		props.put("prop2", "prop2-" + key);
		
		ODocument doc = new ODocument("docnode");
		doc.fromMap(props);
		doc.save();
		/**************node creation ends**********/
		
		System.out.println("node added");

		db.commit();
		db.close();
	}
	
	private static void read() throws Exception
	{
		ODatabaseDocumentTx db = db();
		ORecordIteratorClass<ODocument> iter = db.browseClass(DATATYPE);

		while (iter.hasNext())
			System.out.println("node = " + iter.next().toJSON());
	}
}
