package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.OPartitionedDatabasePool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;

/**
 * This class is written to demonstrate that existsIndex() check returns true
 * even after calling OIndex.delete() method.
 * This file is also available in my github gist
 * 
 * @author K Adithyan
 */
public class IndexDeleteNotWorking 
{
	//tmp directory chosen to avoid accumulating the db in the usable remaining partitions
	static final String URL = "plocal:/tmp/plocaldb";

	//datatype constant
	static final String DATATYPE = "docnode";

	//key field constant
	static final String KEYFIELD = "name";

	private static ODatabaseDocumentTx db()
	{
		System.out.println("starting.....");

		try (ODatabaseDocumentTx base = new ODatabaseDocumentTx(URL))
		{
			if (!base.exists())
			{
				System.out.println("creating....");
				base.create();
				base.getMetadata().getSecurity().createUser("root", "root", "admin");
			}

			base.close();
		}

		OPartitionedDatabasePool pool = new OPartitionedDatabasePool(URL, "root", "root");

		ODatabaseDocumentTx db = pool.acquire();
		
		createIndexIfNeeded(db);

		final OIndexManagerProxy im = db.getMetadata().getIndexManager();
		OIndex index = im.getIndex(indexName());
		System.out.println("index = " + index.getConfiguration().toJSON());

		System.out.println("------------------------------");
		index.delete();
		System.out.println("index deleted");
		System.out.println("------------------------------");

		boolean existsIndex = im.existsIndex(indexName());
		System.out.println("existsIndex after deleting index = " + existsIndex);
		System.out.println("------------------------------");
		
		createIndexIfNeeded(db);
		
		return db;
	}

	private static void createIndexIfNeeded(ODatabaseDocumentTx db)
	{
		OClass dataTypeClass;
		if (!db.getMetadata().getSchema().existsClass(DATATYPE))
		{
			dataTypeClass = db.getMetadata().getSchema().createClass(DATATYPE);
			System.out.println("created class = " + dataTypeClass);
		}
		else
		{
			System.out.println("class exists");
			dataTypeClass = db.getMetadata().getSchema().getClass(DATATYPE);
		}

		OIndexManagerProxy im = db.getMetadata().getIndexManager();
		boolean existsIndex = im.existsIndex(indexName());
		
		System.out.println("existsIndex before creation = " + existsIndex);
		
		if (!existsIndex)
		{
			dataTypeClass.createProperty(KEYFIELD, OType.STRING);
			dataTypeClass.createIndex(indexName(), OClass.INDEX_TYPE.UNIQUE_HASH_INDEX, KEYFIELD);
			System.out.println("index created");
		}
	}
	
	private static String indexName()
	{
		return DATATYPE + "." + KEYFIELD;
	}

	public static void main(String[] args) throws Exception
	{
		db();
	}
}
