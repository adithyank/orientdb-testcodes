package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentPool;
import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.id.ORecordId;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.index.OIndexDefinition;
import com.orientechnologies.orient.core.index.OIndexManagerProxy;
import com.orientechnologies.orient.core.index.OPropertyIndexDefinition;
import com.orientechnologies.orient.core.metadata.schema.OClass;
import com.orientechnologies.orient.core.metadata.schema.OType;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.tx.OTransaction;
import java.util.Date;
import java.util.Map;
import java.util.Random;
import java.util.Set;

/**
 *
 * @author K Adithyan
 */
public class OPatPorDoc
{
	static final String PATTERN_STRING = "PATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPATTERNPA-";
	
	static final String CLASS = "patpor";
	static final String PATTERNNAME = "pattern";
	static final String PORTIONNAME = "portion";
	static final String PATINDEX = CLASS + "." + PATTERNNAME;
	
	static String URL = "plocal:/root/opatpor";
	
	static int PATTERN_COUNT = 1; //1_00_00_000;
	static int PORTION_PER_PATTERN = 2; //400;
	
	static Random random = new Random();
	
	public static void main(String[] args)
	{
		//write();
		read();
	}

	private static ODatabaseDocumentTx db()
	{
		System.out.println("starting.....");
		ODatabaseDocumentTx base = new ODatabaseDocumentTx(URL);
		
		if (!base.exists())
		{
			System.out.println("creating....");
			base.create();
			base.getMetadata().getSecurity().createUser("a", "a", "admin");
		}
		
		System.out.println("post create");
		
		System.out.println("base = " + base);
		base.close();
		
		ODatabaseDocumentPool pool = new ODatabaseDocumentPool(URL, "a", "a");

		ODatabaseDocumentTx db = pool.acquire();
		
		if (db.exists())
			System.out.println("db exists");
		else
			System.out.println("db does not exist");
		
		System.out.println("db = " + db);

		if (!db.getMetadata().getSchema().existsClass(CLASS))
		{
			OClass createClass = db.getMetadata().getSchema().createClass(CLASS);
			System.out.println("create class = " + createClass);
		}
		
		OIndexManagerProxy im = db.getMetadata().getIndexManager();

		if (!im.existsIndex(PATINDEX))
		{
			OIndexDefinition def = new OPropertyIndexDefinition(CLASS, PATTERNNAME, OType.STRING);
			im.createIndex(PATINDEX, "NOTUNIQUE_HASH_INDEX", def, null, null, null, "SBTREE");
			
			System.out.println("index " + PATINDEX + " created");
		}
		else
		{
			System.out.println("index " + PATINDEX + " exists");
		}
		
		long doccount = db.countClass(CLASS);
		System.out.println("doccount of " + CLASS + " = " + doccount);

		return db;
	}

	private static void storeNode(int patNo, int porNo)
	{
		ODocument doc = new ODocument(CLASS);
		doc.fields(PATTERNNAME, PATTERN_STRING + patNo, PORTIONNAME, "PORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPORTIONPO-" + porNo);
		doc.save();
	}
	
	private static void write()
	{
		long t1 = System.currentTimeMillis();
		ODatabaseDocumentTx db = db();
		long t2 = System.currentTimeMillis();
		System.out.println("time taken for db = " + (t2 - t1) + " ms");

		long previousTime = System.currentTimeMillis();
		int GROUPSIZE = 100;
		int count = 0;

		System.out.println("starting...");

		for (int patNo = 0; patNo < PATTERN_COUNT; patNo++)
		{
			long t11 = System.currentTimeMillis();
			db.begin(OTransaction.TXTYPE.OPTIMISTIC);
			long t12 = System.currentTimeMillis();

			for (int porNo = 0; porNo < PORTION_PER_PATTERN; porNo++)
			{
				storeNode(patNo, porNo);
				count++;
			}

			db.commit();
			long t14 = System.currentTimeMillis();

			if ((patNo + 1) % GROUPSIZE == 0)
			{
				long now = System.currentTimeMillis();
				System.out.println(new Date() + " : " + (patNo + 1) + " patterns completed... This time " + count + " docs in " + (now - previousTime) + " ms. Total nodes = " + ((patNo + 1) * PORTION_PER_PATTERN) + " in " + (t14 - t2) + " ms. total time last pattern of " + PORTION_PER_PATTERN + " docs = " + (t14 - t11) + " ms");
				count = 0;
				previousTime = now;
			}
		}

		long t5 = System.currentTimeMillis();
		System.out.println("overall time = " + (t5 - t2) + " ms");
		db.close();
	}

	private static void read()
	{
		ODatabaseDocumentTx db = db();

		OIndexManagerProxy im = db.getMetadata().getIndexManager();
		OIndex<?> index = im.getIndex(CLASS + "." + PATTERNNAME);
		
		long t2 = System.currentTimeMillis();

		long startTime = System.currentTimeMillis();
		
		Object o = index.get(PATTERN_STRING + 3234);
		
		Set<?> s = (Set<?>) o;
		
		System.out.println("size = " + s.size());
		for (Object s1 : s)
		{
			o(s1);
			//System.out.println("item = " + s1 + ", " + o(s1));
		}
		System.out.println("o class = " + o.getClass().getName());
		
		long endTime = System.currentTimeMillis();
		System.out.println("Time taken for one pattern = " + (endTime - startTime) + " ms");
		
		long startTime2 = System.currentTimeMillis();
		
		o = index.get(PATTERN_STRING + 12312);
		
		s = (Set<?>) o;
		
		System.out.println("size = " + s.size());
		for (Object s1 : s)
		{
			o(s1);
			//System.out.println("item = " + s1 + ", " + o(s1));
		}
		System.out.println("o class = " + o.getClass().getName());
		
		long endTime2 = System.currentTimeMillis();
		System.out.println("Time taken for one pattern = " + (endTime2 - startTime2) + " ms");

		
		long startTime3 = System.currentTimeMillis();
		
		o = index.get(PATTERN_STRING + 0);
		
		s = (Set<?>) o;
		
		System.out.println("size = " + s.size());
		for (Object s1 : s)
		{
			o(s1);
			//System.out.println("item = " + s1 + ", " + o(s1));
		}
		System.out.println("o class = " + o.getClass().getName());
		
		long endTime3 = System.currentTimeMillis();
		System.out.println("Time taken for one pattern = " + (endTime3 - startTime3) + " ms");

		
		if (true)
			return;
		
		int count = 0;
		
		for (int i = 0; i < 100000; i++)
		{
			String key = "adithyan-" + random.nextInt(PATTERN_COUNT) + "-" + random.nextInt(PORTION_PER_PATTERN);
			ORecordId rid = (ORecordId) index.get(key);
			ODocument doc = rid.getRecord();

			String[] fns = doc.fieldNames();
			for (String fn : fns)
				doc.field(fn);
		}
		
		long t3 = System.currentTimeMillis();
		System.out.println(new Date() + " : completed reading " + count + " in " + (t3 - t2) + " ms");
	}
	
	static Map<String, Object> o(Object rec)
	{
		ODocument doc = ((ORecordId) rec).getRecord();
		return doc.toMap();
	}
}
