package adi.orientdb.eval;

import com.orientechnologies.orient.core.db.document.ODatabaseDocumentTx;
import com.orientechnologies.orient.core.index.OIndex;
import com.orientechnologies.orient.core.record.impl.ODocument;
import com.orientechnologies.orient.core.sql.query.OSQLSynchQuery;
import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Element;
import com.tinkerpop.blueprints.Index;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.orient.OrientEdge;
import com.tinkerpop.blueprints.impls.orient.OrientGraph;
import com.tinkerpop.blueprints.impls.orient.OrientGraphFactory;
import com.tinkerpop.blueprints.impls.orient.OrientVertex;
import java.util.List;

public class OTester 
{
	private static OrientGraphFactory factory;
	private static OIndex oindex;
	private static Index graphIndex;
	
	static String getNodeName(String name)
	{
		return "DDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD-" + name;
	}
	
	private static OrientVertex getNode(OrientGraph g, String name)
	{
		String fullname = getNodeName(name);
		//System.out.println("getNode() called : " + name);
		Iterable<Vertex> vertices = g.getVertices("invnode.key", fullname);
		boolean available = vertices.iterator().hasNext();
		//System.out.println("available(" + name + ") = " + available);
		
		OrientVertex hub = g.addVertex("class:invnode", "key", fullname);
		return hub;
	}

	public static void main(String[] args)
	{
		//write();
		read();
	}

	public static void read()
	{
		setFactory();
		OrientGraph g = factory.getTx();
		System.out.println("g = " + g);
		System.out.println("node count = " + g.countVertices());

		String[] inputs = {"me2", "me4", "me8"};
		
		String[] edgeNames = {"rel-me-2-ptp-8", "rel-me-5-ptp-4", "rel-me-9-ptp-9", "rel-me-9-ptp-99"};
		
		System.out.println("index.............");
		long t11 = System.currentTimeMillis();

		
		long t12 = System.currentTimeMillis();
		System.out.println("Time taken for INDEX INDEX INDEX  = " + (t12 - t11) + " ms");
		
		
		
		OSQLSynchQuery<OrientEdge> q = new OSQLSynchQuery<>("select from metoptp where out.key = ? and in.key = ?", 1);
		for (String input : inputs)
		{
			long startTime = System.currentTimeMillis();

			String n1s = getNodeName(input);
			String n2s = getNodeName("me-" + input.substring(2) + "-ptp-1");
			
//			OrientVertex v1 = (OrientVertex) g.getVertexByKey("invnode.key", n1s);
//			OrientVertex v2 = (OrientVertex) g.getVertexByKey("invnode.key", n2s);
			
			List<?> result = g.getRawGraph().query(q, n1s, n2s);
			
			System.out.println("query result = " + result);

			for (Object r : result)
			{
				System.out.println("res class = " + r.getClass().getName() + ", doc = " + ((ODocument) r).toJSON());
			}
			
			long endTime = System.currentTimeMillis();
			System.out.println("Time taken for taking other end nodes = " + (endTime - startTime) + " ms. count = " + result.size());
		}
		
		long t1CurrentTimeMillis = System.currentTimeMillis();
		
		OrientVertex v1 = (OrientVertex) g.getVertexByKey("invnode.key", getNodeName("me3"));
		OrientVertex v2 = (OrientVertex) g.getVertexByKey("invnode.key", getNodeName("me-3-ptp-1"));
		Iterable<Edge> edges = v1.getEdges(v2, Direction.OUT, "metoptp");
		
		System.out.println("vvvvvvvvvvv= " + v1.getId());
		for (Edge edge : edges)
		{
			OrientEdge e = (OrientEdge) edge;
			System.out.println("TAKEN EDGE = " + e.getRecord().toJSON());
		}
		
		long t2CurrentTimeMillis = System.currentTimeMillis();
		System.out.println("Time taken for TAKEN EDGE = " + (t2CurrentTimeMillis - t1CurrentTimeMillis) + " ms");
		
		
		System.out.println("done....");
	}

	private static void setFactory()
	{
		System.out.println("starting.......");
		long t1 = System.currentTimeMillis();
		String url = "plocal:/root/successdb";
		
		OrientGraph og = new OrientGraph(url);
		
		System.out.println("og = " + og);
		
		if (true)
			return;
		
		factory = new OrientGraphFactory(url).setupPool(1, 4);
	
		
		
		long t2 = System.currentTimeMillis();
		System.out.println("time taken for gettting graph factory = " + (t2 - t1) + " ms");
		OrientGraph g = factory.getTx();

		long t3 = System.currentTimeMillis();
		System.out.println("time taken for gettting graph object = " + (t3 - t2) + " ms");
		ODatabaseDocumentTx gg = g.getRawGraph();

		//g.dropKeyIndex("key", Vertex.class);
		//g.dropKeyIndex("key", Edge.class);
		Iterable<Index<? extends Element>> indices = g.getIndices();
		
		for (Index<? extends Element> i : indices)
		{
			System.out.println("index = " + i);
		}

		boolean exists = gg.getMetadata().getIndexManager().existsIndex("invnode.key");
		System.out.println("index exists = " + exists);
		if (!exists)
		{
			g.createKeyIndex("key", Vertex.class, new Parameter("class", "invnode"), new Parameter("type", "unique_hash_index"));
			System.out.println("v index created");
			g.createKeyIndex("key", Edge.class, new Parameter("class", "metoptp"), new Parameter("type", "unique_hash_index"));
			System.out.println("e index created");
		}			
		
		OIndex<?> vindex = gg.getMetadata().getIndexManager().getIndex("invnode.key");
		if (vindex != null)
			System.out.println("index = " + vindex.getName() + "|" + vindex.getType() + "|" + vindex.getConfiguration().toJSON());
		
		System.out.println("#########################################");
		oindex = gg.getMetadata().getIndexManager().getIndex("metoptp.key");
		if (oindex != null)
			System.out.println("index = " + oindex.getName() + "|" + oindex.getType() + "|" + oindex.getConfiguration().toJSON());

		System.out.println("#########################################");
		graphIndex = g.getIndex("metoptp", Edge.class);
		if (graphIndex != null)
			System.out.println("graphIndex = " + graphIndex);
		System.out.println("#########################################");
		
		long t4  = System.currentTimeMillis();
		System.out.println("time taken for creating index = " + (t4 - t3) + " ms");
		System.out.println("total time for getGraph = " + (t4 - t1) + " ms");
		
		//if (true)
		//	System.exit(0);
	}

	public static void write()
	{
		setFactory();
		long totals = System.currentTimeMillis();
		OrientGraph g = null;
		//g.createEdgeType("hub-spoke");
		try
		{
			int PORT_COUNT = 400;
			int GROUP = 100;
			long objectCreationTime = 0;
			long  commitTime = 0;
			long lastPrintTime = System.currentTimeMillis();
			
			int totalNodes = 0;
			int totalEdges = 0;
			
			System.out.println("starting....");
			
			for (int me = 0; me < 10; me++)
			{
				boolean print = me != 0 && me % GROUP == 0;
				
				if (print)
				{
					long now = System.currentTimeMillis();
					System.out.println((GROUP * PORT_COUNT + 1) + " nodes and " + (GROUP * PORT_COUNT) + " edges : obj creation time = " + objectCreationTime + ", commit time = " + commitTime + " ms. time between last print and this print = " + (now - lastPrintTime) + " ms. cumulative time = " + ((System.currentTimeMillis() - totals) / 1000) + " seconds. total nodes = " + totalNodes + ", total edges = " + totalEdges);
					lastPrintTime = now;
					commitTime = 0;
					objectCreationTime = 0;
				}

				if (print)
					System.out.println("------------------------------------------------------starting me --------" + me);

				long t3 = System.currentTimeMillis();
				
				g = factory.getTx();
				
				OrientVertex node = getNode(g, "me" + me);
				totalNodes++;
				for (int i = 0; i < PORT_COUNT; i++)
				{
					OrientVertex s = getNode(g, "me-" + me + "-ptp-" + i);
					totalNodes++;
					String en = "rel-me-" + me + "-ptp-" + i;
					node.addEdge("metoptp", s, new Object[] {"key", en, "layerrate", "STM1"});
					totalEdges++;
				}

				long t4 = System.currentTimeMillis();

				objectCreationTime += t4 - t3;

				g.commit();
				g.shutdown();
				long t5 = System.currentTimeMillis();
				commitTime += t5 -t4;
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
			
			if (g != null)
				g.rollback();
		}

		long totale = System.currentTimeMillis();
		System.out.println("total time taken = " + (totale - totals) + " ms");
	
	}
}
